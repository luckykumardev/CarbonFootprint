var CarbonFootprintCore = function(t, r) {
    this.settingsProvider = t, this.helper = r, this.treeGrowthPerYear = 8.3
};
CarbonFootprintCore.IMPGAL_TO_L = 4.54609, CarbonFootprintCore.USGAL_TO_L = 3.785411784, CarbonFootprintCore.MI_TO_KM = 1.609344, CarbonFootprintCore.FT_TO_KM = 3048e-7, CarbonFootprintCore.KG_TO_LBS = 2.204622621848775, CarbonFootprintCore.prototype.computeFootprint = function(t, r) {
    var o;
    return o = "t" == r ? t * this.settingsProvider.getPTCarbonEmission() : t * this.settingsProvider.getCarbonEmission()
}, CarbonFootprintCore.prototype.footprintToString = function(t) {
    var r = this.settingsProvider.getCarbonEmissionUnit();
    return t < 1 && "kg" == r ? (t *= 1e3, r = "g") : "lbs" == r && (t *= CarbonFootprintCore.KG_TO_LBS, t < 1 && (t *= 16, r = "oz")), t = t.toPrecision(3), "" + t + r + " CO<sub>2</sub>"
}, CarbonFootprintCore.prototype.computeTrees = function(t) {
    var r = t / this.treeGrowthPerYear;
    return r = Math.round(100 * r) / 100
}, CarbonFootprintCore.prototype.otherGasesString = function(t) {
    return this.settingsProvider.getGHGEmission() >= 0 ? "CH₄: " + (1e3 * this.settingsProvider.getCH4Emission() * t).toFixed(3) + "g CO₂e,  N₂O: " + (1e3 * this.settingsProvider.getN2OEmission() * t).toFixed(3) + "g CO₂e,  GHG: " + (this.settingsProvider.getGHGEmission() * t).toFixed(3) + "kg CO₂e\n" : ""
}, CarbonFootprintCore.prototype.treesToString = function(t) {
    return t > 1 ? "You will need " + Math.round(t) + " tropical trees growing for 1 year to capture that much CO2! (or " + Math.round(12 * t) + " trees growing for 1 month, or " + Math.round(365 * t) + " trees growing for 1 day)" : 12 * t > 1 ? "You will need " + Math.round(12 * t) + " tropical trees growing for 1 month to capture that much CO2! (or " + Math.round(365 * t) + " trees growing for 1 day)" : 365 * t > 1 ? "You will need " + Math.round(365 * t) + " tropical trees growing for 1 day to capture that much CO2!" : "Your Carbon Emission is almost nil. Great going!"
}, CarbonFootprintCore.prototype.createHTMLElement = function(t, r) {
    var o = document.createElement("div"),
        e = this.treesToString(this.computeTrees(t)),
        n = this.otherGasesString(r),
        i = n + e,
        a = this.helper.getFilePath("pages/knowMore.html");
    return o.setAttribute("id", "carbon-footprint-label"), o.innerHTML = "<a href=" + a + " target='_blank' title='" + i + "' class='carbon' id='carbon'>" + this.footprintToString(t) + '<svg id="quest_mark_icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92"><path d="M45.4 0C20 0.3-0.3 21.2 0 46.6c0.3 25.4 21.2 45.7 46.6 45.4 25.4-0.3 45.7-21.2 45.4-46.6C91.7 20 70.8-0.3 45.4 0zM45.3 74l-0.3 0c-3.9-0.1-6.7-3-6.6-6.9 0.1-3.8 2.9-6.5 6.7-6.5l0.2 0c4 0.1 6.7 3 6.6 6.9C51.9 71.3 49.1 74 45.3 74zM61.7 41.3c-0.9 1.3-2.9 2.9-5.5 4.9l-2.8 1.9c-1.5 1.2-2.5 2.3-2.8 3.4 -0.3 0.9-0.4 1.1-0.4 2.9l0 0.5H39.4l0-0.9c0.1-3.7 0.2-5.9 1.8-7.7 2.4-2.8 7.8-6.3 8-6.4 0.8-0.6 1.4-1.2 1.9-1.9 1.1-1.6 1.6-2.8 1.6-4 0-1.7-0.5-3.2-1.5-4.6 -0.9-1.3-2.7-2-5.3-2 -2.6 0-4.3 0.8-5.4 2.5 -1.1 1.7-1.6 3.5-1.6 5.4v0.5H27.9l0-0.5c0.3-6.8 2.7-11.6 7.2-14.5C37.9 18.9 41.4 18 45.5 18c5.3 0 9.9 1.3 13.4 3.9 3.6 2.6 5.4 6.5 5.4 11.6C64.4 36.3 63.5 38.9 61.7 41.3z" /></svg>', o.onh, o
}, CarbonFootprintCore.prototype.createFootprintElement = function(t) {
    var r = this.computeFootprint(t, "d"),
        o = this.createHTMLElement(r, t);
    return o
}, CarbonFootprintCore.prototype.createPTFootprintElement = function(t) {
    var r = this.computeFootprint(t, "t"),
        o = this.createHTMLElement(r);
    return o
}, CarbonFootprintCore.prototype.computeTravelCost = function(t) {
    var r = this.settingsProvider.getTravelRate() * t;
    return r
}, CarbonFootprintCore.prototype.createTravelCostElement = function(t) {
    var r = document.createElement("div");
    return r.innerHTML = "<div class=travelCost id=travelCost>" + this.computeTravelCost(t).toFixed(2).toString() + " " + this.settingsProvider.getCurrency() + "</div>", r
}, CarbonFootprintCore.prototype.getDistanceFromStrings = function(t, r) {
    t = t.trim();
    var o = t.lastIndexOf(","),
        e = t.length - o;
    return e <= 3 && (t = t.substr(0, o) + "." + t.substr(o + 1)), t = parseFloat(t.replace(/,/g, "").replace(" ", "")), r.match(/\bm\b/) || r.match(/\s\u043C,/) ? t /= 1e3 : r.match(/\bmi\b/) || r.match(/\bMeile(n?)\b/) || r.match(/\bmil/) || r.match(/\bm\u00ed/) || r.match(/\bmaili(a?)/) || r.match(/\bmylia/) || r.match(/\bmigli(o|a)/) || r.match(/\bmérföld/) || r.match(/\bjūdze(s?)/) || r.match(/\bμίλι/) || r.match(/\bмілі/) || r.match(/\bmi(j|i)l/) || r.match(/\u043C\u0438\u043B/) ? t *= CarbonFootprintCore.MI_TO_KM : (r.match(/\bft\b/) || r.match(/\bp\u00E9s\b/) || r.match(/\u0444\u0443\u0442/)) && (t *= CarbonFootprintCore.FT_TO_KM), t
};